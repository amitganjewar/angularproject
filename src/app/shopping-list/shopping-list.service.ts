import {Ingredient} from '../shared/Ingredient.model';
import {Subject} from 'rxjs';

export class ShoppingListService{

    ingredientsChanged = new Subject<Ingredient[]>();
    startedEditing = new Subject<number>();

    ingredients: Ingredient[] = [
        new Ingredient('Apples', 10),
        new Ingredient('Tomatoes', 15)
      ];

    getIngredientByIndex(index : number){
        return this.ingredients[index];
    }
    addIngradiants(ingredient : Ingredient){
        this.ingredients.push(ingredient);
        this.ingredientsChanged.next(this.ingredients.slice());
    }

    updateIngredeint (index: number, ingredient: Ingredient){
        this.ingredients[index]=ingredient;
        this.ingredientsChanged.next(this.ingredients.slice());
    }

    deleteIngredient (index: number){
        this.ingredients.splice(index,1);
        this.ingredientsChanged.next(this.ingredients.slice());
    }
}