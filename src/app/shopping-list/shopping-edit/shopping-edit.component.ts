import { Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Subscription} from 'rxjs';

import {Ingredient} from '../../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list.service';


@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

  @ViewChild('f') slForm : NgForm;
  subscription: Subscription;
  editMode = false;
  editedItemIndex : number;
  editeditem: Ingredient;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    this.subscription = this.shoppingListService.startedEditing
    .subscribe(
      (index: number) => {
        this.editMode = true;
        this.editedItemIndex = index;
        this.editeditem = this.shoppingListService.getIngredientByIndex(index);
        this.slForm.setValue({
          name: this.editeditem.name,
          amount: this.editeditem.amount
        })
      }
    );
  }

  onSubmit(form: NgForm) {
    const value = form.value;
    const newIngrdiant = new Ingredient(value.name, value.amount);
    if (this.editMode) {
      this.shoppingListService.updateIngredeint(this.editedItemIndex, newIngrdiant);
    }
    else {
      this.shoppingListService.addIngradiants(newIngrdiant);
    }
    this.editMode = false;
    form.reset();
  }

  onClear(){
    this.slForm.reset();
    this.editMode = false;
  }

  onDelete(){
    this.shoppingListService.deleteIngredient(this.editedItemIndex);
    this.onClear();
  }
  

  ngOnDestroy(){
    this.subscription.unsubscribe();
    }
}
