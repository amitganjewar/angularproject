import {Subject} from 'rxjs';
import {Recipe} from './recipe.model';
import {Ingredient} from '../shared/ingredient.model';

export class RecipesService{

  recipesChanged = new Subject<Recipe[]>();

   private recipes: Recipe [] = [
        new Recipe('Recipe 1',
                   'This is Recipe 1 description', 
                   'https://cdn.pixabay.com/photo/2016/06/15/19/09/food-1459693_960_720.jpg',
                  [
                    new Ingredient('egg', 1),
                    new Ingredient('Tomato', 3),
                    new Ingredient('Onion', 2)
                  ]),
        new Recipe('Recipe 2',
                   'This is Recipe 2 description', 
                   'https://cdn.pixabay.com/photo/2016/06/15/19/09/food-1459693_960_720.jpg',
                  [
                    new Ingredient('cucumber', 5),
                    new Ingredient('Garlic', 7),
                    new Ingredient('Meat', 1)
                  ])
      ];

  getRecipe(){
     return this.recipes.slice();
  }

  getRecipeById(index:number){
    return this.recipes[index];

  }

  addRecipe(newRecipe: Recipe){
    this.recipes.push(newRecipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe:Recipe){
    this.recipes[index] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());
  }
}