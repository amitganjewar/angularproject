import { Component, OnInit, OnDestroy } from '@angular/core';
import {Router, ActivatedRoute,} from '@angular/router';
import {Subscription} from 'rxjs';

import {Recipe} from '../recipe.model';
import {RecipesService} from '../recipes.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {
  recipesChangedSubscription: Subscription;
  recipes: Recipe [];
  constructor(private recipesService: RecipesService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.recipesChangedSubscription =  this.recipesService.recipesChanged
    .subscribe(
      (recipes: Recipe[]) => {
        this.recipes = recipes;
      }
    );
    this.recipes = this.recipesService.getRecipe();
   
  }

  onSelectedRecipe(recipe: Recipe){
  }

  onNewRecipe(){
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  ngOnDestroy(){
    this.recipesChangedSubscription.unsubscribe();
  }

}
