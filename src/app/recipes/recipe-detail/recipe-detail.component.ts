import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {Recipe} from '../Recipe.model';
import {ShoppingListService} from '../../shopping-list/shopping-list.service';
import {RecipesService} from '../recipes.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  recipeToDisplay: Recipe;
  id :number;

  constructor( private shoppingListService: ShoppingListService,
               private recipesService: RecipesService,
               private route: ActivatedRoute,
               private router : Router,              
              ) { }

  ngOnInit() {
    this.route.params
    .subscribe(
      (params : Params) => {
        this.id= +params['id'];
        this.recipeToDisplay = this.recipesService.getRecipeById(this.id);
      }
    );
  }

  addToShoppingList(){
    this.recipeToDisplay.ingredients.forEach(element => {
      this.shoppingListService.addIngradiants(element);
    });
    //this.shoppingListService.addIngradiants(this.recipeToDisplay.ingredients[0]);
  }

  onEditRecipe(){
    this.router.navigate(['edit'], {relativeTo: this.route})
  }

}
