import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
selector: '[appDropdown]'
})
export class DropdownDirective{

    @HostBinding('class.open') openIt = false;
    @HostListener('click') toggleOpen(){
        this.openIt = !this.openIt;
    }
}